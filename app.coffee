express = require('express')
path = require('path')
app = express()
five = require('johnny-five')
server = require('http').createServer(app)
io = require('socket.io').listen(server)

server.listen(8989)

app.use express.static(path.join(__dirname, '/public'))
app.use express.static(path.join(__dirname, '/'))

app.get '/', (req, res)->
	res.sendfile (__dirname + '/public/index.htm')

obj = { val: 0 }
users = 0
led = null
piezo = null
boardReady = false
lcd = null
lcdReady = false
light = 0

io.sockets.on 'connection', (socket)->

	users += 1
	console.log 'users:', users

	# userConnected()
	printLcd 'users:' + users

	socket.on 'slider:change', (data)->
		console.log data
		# led.on()
		# console.log led
		# if led.value is 255
		# 	led.off()
		# else
		# 	led.on()

		obj = data
		io.sockets.emit 'slider:update', obj

	socket.on 'disconnect', ()->
		users -= 1
		# lcd.clear()
		# piezo.tone 2000, 200
		# setTimeout ()->
		# 	console.log 'disconnect..'
		# 	printLcd('users:' + users)
		# , 1000
		printLcd 'users:' + users
		console.log 'users:', users

	socket.on 'bg:change', (data)->
		console.log data
		# led.off()
		io.sockets.emit 'bg:change', data

	socket.on 'msg', (data)->
		printLcd (data.msg)
		led.strobe(50)
		setTimeout ()->
			led.stop()
			chkLight()
		, 500

	socket.on 'light', (data)->
		light = data.light
		chkLight()

board = new five.Board()

board.on 'ready', ()->
	console.log 'board is ready..'
	led = new five.Led({ pin: 13 })
	# piezo = new five.Piezo(13)


	boardReady = true

	led.off()

	lcd = new five.LCD({ pins: [7, 8, 9, 10, 11, 12] })

	lcd.on 'ready', ()->
		console.log 'lcd ready'
		lcdReady = true

		lcd.useChar 'heart'
		lcd.useChar 'smile'
		lcd.useChar 'check'

		lcd.clear().print(':check:')

	@.repl.inject ({ lcd: lcd, led: led }) # use the command line to write to lcd..

playSong = ()->
	if boardReady
		piezo.song 'cfaC aC ', '11111142'

printLcd = (msg)->
	if (boardReady && lcdReady)
		lcd.clear()
		msg = msg.trim()
		if (msg.length > 16)
			lcd.print msg.substr(0, 16)
			lcd.cursor(1, 0)
			lcd.print msg.substr(16)
		else
			lcd.print msg

userConnected = (msg)->
	if (boardReady && lcdReady)
		printLcd('u')
		# piezo.onSongComplete = ()->
		# 	console.log 'song complete..'
		# 	setTimeout ()->
		# 		console.log 'SET LCD!!!'
		# 		printLcd('users:' + users)
		# 	, 1000

		# lcd.clear()
		# playSong()

chkLight = ()->
	if (light == 'on')
		led.on()
	else
		led.off()
