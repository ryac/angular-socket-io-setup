(function() {
  'use strict';
  angular.module('skl.controller', []).controller('MainCtrl', [
    '$scope', 'socket', function($scope, socket) {
      $scope.sliderVal = 0;
      $scope.bodyStyle = {
        background: 'white'
      };
      $scope.onSliderChange = function() {
        return socket.emit('slider:change', {
          val: $scope.sliderVal
        });
      };
      $scope.onBtnClick = function() {
        $scope.bodyStyle = {
          background: 'yellow'
        };
        return socket.emit('slider:change', {
          val: 12
        });
      };
      $scope.onBtnDown = function(color) {
        socket.emit('bg:change', {
          background: color
        });
        return console.log('onBtnDown', color);
      };
      $scope.onBtnUp = function(color) {
        return socket.emit('bg:change', {
          background: 'white'
        });
      };
      $scope.sendMsg = function() {
        console.log($scope.msg);
        return socket.emit('msg', {
          msg: $scope.msg
        });
      };
      $scope.btnToggle = function(toggle) {
        $scope.toggle = toggle;
        socket.emit('light', {
          light: toggle
        });
        return console.log($scope.toggle);
      };
      socket.on('news', function(data) {
        console.log('fuck ya', data);
        return $scope.sliderVal = data.val;
      });
      socket.on('slider:update', function(data) {
        console.log('on update -->', data.val);
        return $scope.sliderVal = data.val;
      });
      return socket.on('bg:change', function(data) {
        console.log('data from server-->', data);
        return $scope.bodyStyle = data;
      });
    }
  ]);

}).call(this);
