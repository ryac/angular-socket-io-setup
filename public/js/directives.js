(function() {
  'use strict';
  angular.module('skl.directives', []).directive('ryeBody', [
    '$window', function($window) {
      return {
        restrict: 'A',
        scope: true,
        link: function(scope, element, attrs) {
          console.log(element);
          console.log(scope.color);
          element.css({
            background: scope.color
          });
          return this;
        }
      };
    }
  ]);

}).call(this);
