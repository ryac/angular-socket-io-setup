(function() {
  'use strict';
  angular.module('skl.service-socket-io', []).factory('socket', function($rootScope) {
    var socket;
    socket = io.connect('http://24.86.15.224:8989');
    return {
      on: function(evntName, cb) {
        return socket.on(evntName, function() {
          var args;
          args = arguments;
          return $rootScope.$apply(function() {
            return cb.apply(socket, args);
          });
        });
      },
      emit: function(evntName, data, cb) {
        return socket.emit(evntName, data, function() {
          var args;
          args = arguments;
          return $rootScope.$apply(function() {
            if (cb) {
              return cb.apply(socket, args);
            }
          });
        });
      }
    };
  });

}).call(this);
