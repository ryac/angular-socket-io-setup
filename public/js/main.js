(function() {
  'use strict';
  angular.module('Skeleton', ['ngTouch', 'angular-gestures', 'skl.controller', 'skl.directives', 'skl.service-socket-io']);

}).call(this);
