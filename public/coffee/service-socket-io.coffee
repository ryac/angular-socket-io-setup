'use strict';

angular
	.module('skl.service-socket-io', [])
	.factory 'socket', ($rootScope)->
		socket = io.connect('http://24.86.15.224:8989')
		return {
			on: (evntName, cb)->
				socket.on evntName, ()->
					args = arguments
					$rootScope.$apply ()->
						cb.apply(socket, args)
			,
			emit: (evntName, data, cb)->
				socket.emit evntName, data, ()->
					args = arguments
					$rootScope.$apply ()->
						if (cb)
							cb.apply(socket, args)
		}