'use strict';

angular
	.module('skl.controller', [])
	.controller('MainCtrl', [
		'$scope'
		'socket'
		($scope, socket)->

			$scope.sliderVal = 0
			$scope.bodyStyle = { background: 'white' }

			$scope.onSliderChange = ()->
				socket.emit 'slider:change', { val: $scope.sliderVal }

			$scope.onBtnClick = ()->
				$scope.bodyStyle = { background: 'yellow' }
				socket.emit 'slider:change', { val: 12 }

			$scope.onBtnDown = (color)->
				# $scope.bodyStyle = { background: color }
				# $scope.bodyStyle.background = color
				socket.emit 'bg:change', { background: color }
				console.log 'onBtnDown', color

			$scope.onBtnUp = (color)->
				socket.emit 'bg:change', { background: 'white' }
				# $scope.bodyStyle.background = 'white'

			$scope.sendMsg = ()->
				console.log $scope.msg
				socket.emit 'msg', { msg: $scope.msg }

			$scope.btnToggle = (toggle)->
				$scope.toggle = toggle
				socket.emit 'light', { light: toggle }
				console.log $scope.toggle

			socket.on 'news', (data)->
				console.log 'fuck ya', data
				$scope.sliderVal = data.val

			socket.on 'slider:update', (data)->
				console.log 'on update -->', data.val
				$scope.sliderVal = data.val

			socket.on 'bg:change', (data)->
				console.log 'data from server-->', data
				$scope.bodyStyle = data

	])