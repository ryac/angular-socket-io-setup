module.exports = (grunt)->

	# Tasks
	grunt.initConfig({

		# ----------------------
		# Coffee
		# ----------------------
		coffee:
			dev:
				expand: true
				cwd: 'public/coffee/'
				src: ['**.coffee', '**/*.coffee']
				dest: 'public/js/'
				ext: '.js'

		# ----------------------
		# Watch
		# ----------------------
		watch:
			scripts:
				files: 'public/coffee/**/*.coffee'
				tasks: ['coffee']
			# compile_js:
			# 	files: 'app/webroot/js/main.js'
			# 	tasks: ['concat', 'uglify']

	})

	# Plugins
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks)

	# Commands
	grunt.registerTask 'default', ['']

	return grunt